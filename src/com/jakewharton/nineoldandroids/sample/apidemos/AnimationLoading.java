/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jakewharton.nineoldandroids.sample.apidemos;

// Need the following import to get access to the app resources, since this
// class is in a sub-package.
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorInflater;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.jakewharton.nineoldandroids.sample.R;

/**
 * This application demonstrates loading Animator objects from XML resources.
 */
public class AnimationLoading extends Activity {

    private static final int DURATION = 1500;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_loading);
        LinearLayout container = (LinearLayout) findViewById(R.id.container);
        final MyAnimationView animView = new MyAnimationView(this);
        container.addView(animView);

        Button starter = (Button) findViewById(R.id.startButton);
        starter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                animView.startAnimation();
            }
        });
    }

    public class MyAnimationView extends View implements ValueAnimator.AnimatorUpdateListener {

        private static final float BALL_SIZE = 50;

        public final ArrayList<ShapeHolder> balls = new ArrayList<ShapeHolder>();
        Animator animation = null;

        public MyAnimationView(Context context) {
            super(context);
            addBall(50, 50);
            addBall(150, 50);
            addBall(250, 50);
            addBall(300, 50, Color.GREEN);
        }

        private void createAnimation() {
            Context appContext = AnimationLoading.this;

            if (animation == null) {
            	// 从配置文件加载objectAnimator动画 (Y方向向下移动200,并且回滚)
            	/*
            	 <objectAnimator xmlns:android="http://schemas.android.com/apk/res/android"
    					android:duration="1000"
    					android:valueTo="200"
    					android:valueType="floatType"
    					android:propertyName="y"
    					android:repeatCount="1"
    					android:repeatMode="reverse"/>
            	 */
                ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(appContext, R.anim.object_animator);
                anim.addUpdateListener(this);
                anim.setTarget(balls.get(0));

                // 从配置文件加载animator动画 (先渐隐，再回滚)
                /*
                <animator xmlns:android="http://schemas.android.com/apk/res/android"
    				android:duration="1000"
    				android:valueFrom="1"
    				android:valueTo="0"
    				android:valueType="floatType"
    				android:repeatCount="1"
    				android:repeatMode="reverse"/>
                 */
                ValueAnimator fader = (ValueAnimator) AnimatorInflater.loadAnimator(appContext, R.anim.animator);
                fader.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        balls.get(1).setAlpha((Float) animation.getAnimatedValue());
                    }
                });

                // 从配置文件加载组动画(x移动到200，y移动到400，并且回滚)
                /*
                <set>
    				<objectAnimator xmlns:android="http://schemas.android.com/apk/res/android"
        				android:duration="1000"
        				android:valueTo="200"
        				android:valueType="floatType"
        				android:propertyName="x"
        				android:repeatCount="1"
        				android:repeatMode="reverse"/>
    				<objectAnimator xmlns:android="http://schemas.android.com/apk/res/android"
        				android:duration="1000"
        				android:valueTo="400"
        				android:valueType="floatType"
        				android:propertyName="y"
        				android:repeatCount="1"
        				android:repeatMode="reverse"/>
				</set>
                */
                AnimatorSet seq = (AnimatorSet) AnimatorInflater.loadAnimator(appContext, R.anim.animator_set);
                seq.setTarget(balls.get(2));

                // 颜色变化动画
                /*
                <objectAnimator xmlns:android="http://schemas.android.com/apk/res/android"
    				android:duration="1000"
    				android:valueFrom="#0f0"
    				android:valueTo="#00ffff"
    				android:propertyName="color"
    				android:repeatCount="1"
    				android:repeatMode="reverse"/>
                */
                ObjectAnimator colorizer = (ObjectAnimator) AnimatorInflater.loadAnimator(appContext, R.anim.color_animator);
                colorizer.setTarget(balls.get(3));

                // 四个球同时开始动画
                animation = new AnimatorSet();
                ((AnimatorSet) animation).playTogether(anim, fader, seq, colorizer);
            }
        }

        public void startAnimation() {
            createAnimation();
            animation.start();
        }

        private ShapeHolder createBall(float x, float y) {
            OvalShape circle = new OvalShape();
            circle.resize(BALL_SIZE, BALL_SIZE);
            ShapeDrawable drawable = new ShapeDrawable(circle);
            ShapeHolder shapeHolder = new ShapeHolder(drawable);
            shapeHolder.setX(x);
            shapeHolder.setY(y);
            return shapeHolder;
        }

        private void addBall(float x, float y, int color) {
            ShapeHolder shapeHolder = createBall(x, y);
            shapeHolder.setColor(color);
            balls.add(shapeHolder);
        }

        private void addBall(float x, float y) {
            ShapeHolder shapeHolder = createBall(x, y);
            int red = (int)(100 + Math.random() * 155);
            int green = (int)(100 + Math.random() * 155);
            int blue = (int)(100 + Math.random() * 155);
            int color = 0xff000000 | red << 16 | green << 8 | blue;
            Paint paint = shapeHolder.getShape().getPaint();
            int darkColor = 0xff000000 | red/4 << 16 | green/4 << 8 | blue/4;
            RadialGradient gradient = new RadialGradient(37.5f, 12.5f,
                    50f, color, darkColor, Shader.TileMode.CLAMP);
            paint.setShader(gradient);
            balls.add(shapeHolder);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            for (ShapeHolder ball : balls) {
                canvas.translate(ball.getX(), ball.getY());
                ball.getShape().draw(canvas);
                canvas.translate(-ball.getX(), -ball.getY());
            }
        }

        public void onAnimationUpdate(ValueAnimator animation) {

            invalidate();
            ShapeHolder ball = balls.get(0);
            ball.setY((Float)animation.getAnimatedValue());
        }
    }
}